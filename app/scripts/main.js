$(document).ready(function() {

  $('#owl-reviews').owlCarousel({

    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false

  });

  $('#owl-seo').owlCarousel({
    pagination: false,
    navigation: true, // Show next and prev buttons
    navigationText: [
      '<img src=\'images/owl-left.png\'></img>',
      '<img src=\'images/owl-right.png\'></img>'
    ],
    slideSpeed: 300,
    paginationSpeed: 400,
    singleItem: true

    // "singleItem:true" is a shortcut for:
    // items : 1, 
    // itemsDesktop : false,
    // itemsDesktopSmall : false,
    // itemsTablet: false,
    // itemsMobile : false
  });

});